const getPostCssBreakpoints = (breakpoints) => {
	let newObj = {};

	Object.keys(breakpoints).forEach((key) => {
		if (breakpoints[key] !== "Infinity") {
			newObj = {
				[`--breakpoint-${key}`]: key.includes("max")
					? `(max-width: ${breakpoints[key]}px)`
					: `(min-width: ${breakpoints[key]}px)`,
				...newObj,
			};
		}
	});

	return newObj;
};

const breakpoints = {
	xs: 480,
	sm: 700,
	md: 1000,
	lg: 1300,
	xl: 1600,
	xxl: "Infinity",
	"xs-max": 699,
	"sm-max": 999,
};

export const customMediaQueriesBreakpoints = getPostCssBreakpoints(breakpoints);

export default breakpoints;
