export const DEFAULT_VALUE = {
	name: "",
	url: "",
	icon: "",
	background: {
		publicId: "",
		alt: "",
	},
};

export const state = () => ({
	list: [
		{
			name: "Spotify",
			url: "https://open.spotify.com/artist/1LdoXWB993Sa3SSNzD6RPJ?si=OW66mFA8R3Sv04JaHfCmbg",
			icon: ["fab", "spotify"],
			background: {
				publicId: "royce/spotify.jpg",
				alt: "Royce on Spotify",
			},
		}, {
			name: "iTunes",
			url: "https://itunes.apple.com/be/artist/royce/1171178416",
			icon: ["fab", "itunes-note"],
			background: {
				publicId: "royce/itunes.jpg",
				alt: "Royce on iTunes",
			},
		}, {
			name: "Bandcamp",
			url: "https://roycetheband.bandcamp.com/releases",
			icon: ["fab", "bandcamp"],
			background: {
				publicId: "royce/bandcamp.jpg",
				alt: "Royce on Bandcamp",
			},
		}, {
			name: "Youtube",
			url: "https://www.youtube.com/channel/UCqI1Wks4V_gBcuOyyMOrknw",
			icon: ["fab", "youtube"],
			background: {
				publicId: "royce/youtube.jpg",
				alt: "Royce on Youtube",
			},
		},
	],
});
