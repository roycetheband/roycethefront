/* eslint-disable no-shadow, no-param-reassign, no-unused-vars, no-console */
import { getField, updateField } from "vuex-map-fields";

export const GET_PRODUCTS = "GET_PRODUCTS";

export const defaultBilling = {
	firstName: null,
	lastName: null,
	address1: null,
	address2: null,
	city: null,
	state: null,
	postcode: null,
	country: null,
	email: null,
	phone: null,
};

export const defaultShipping = {
	firstName: null,
	lastName: null,
	address1: null,
	address2: null,
	city: null,
	state: null,
	postcode: null,
	country: null,
};

const defaultSingle = {
	paymentMethod: null,
	paymentMethodTitle: null,
	setPaid: false,
	shippingEqualsBilling: true,
	billing: { ...defaultBilling },
	shipping: { ...defaultShipping },
	lineItems: [
		{
			productId: 93,
			quantity: 2,
		},
		{
			productId: 22,
			variationId: 23,
			quantity: 1,
		},
	],
	shippingLines: [
		{
			methodId: "flat_rate",
			methodTitle: "Flat Rate",
			total: 10,
		},
	],
};

export const state = () => ({
	list: [],
	single: { ...defaultSingle },
});

export const mutations = {
	updateField,
	[GET_PRODUCTS](state, products) {
		state.list = products;
	},
};

export const actions = {
	async getList({ commit }, { $axios = this.$axios, error = console.error } = {}) {
		try {
			const products = await $axios.$get("/wc/products");
			commit(GET_PRODUCTS, products);
		} catch (e) {
			error(e);
		}
	},
};

export const getters = {
	getField,
	list: state => state.list,
	listByCategoryId: state => categoryId => state.list.filter(p => p.categories.some(c => c.id === categoryId)),
	byId: state => id => state.list.find(c => c.id === id),
	bySlug: state => slug => state.list.find(c => c.slug === slug),
	stockById: state => (id) => {
		const {
			inStock,
			manageStock,
			stockQuantity,
			backordersAllowed,
		} = state.list.find(c => c.id === id);

		return {
			inStock,
			manageStock,
			stockQuantity,
			backordersAllowed,
		};
	},
};
