/* eslint-disable no-shadow, no-param-reassign, no-unused-vars */

export const TOGGLE_MENU = "toggleMenu";

export const state = () => ({
	visible: false,
});

export const mutations = {
	[TOGGLE_MENU](state) {
		state.visible = !state.visible;
	},
};

export const actions = {
	[TOGGLE_MENU]({ commit }) {
		commit(TOGGLE_MENU);
	},
};
