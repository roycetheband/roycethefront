/* eslint-disable no-shadow, no-param-reassign, no-console */

export const GET_CONTENT = "GET_CONTENT";
export const GET_CONTENT_SET_LOADING = "GET_CONTENT_SET_LOADING";
export const GET_TOTALS = "GET_TOTALS";
export const GET_TOTALS_SET_LOADING = "GET_TOTALS_SET_LOADING";
export const CLEAR = "CLEAR";

export const ADD_PRODUCT = "ADD_PRODUCT";
export const ADD_PRODUCT_SET_LOADING = "ADD_PRODUCT_SET_LOADING";

export const REMOVE_PRODUCT = "REMOVE_PRODUCT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const CRUD_PRODUCT_SET_LOADING = "CRUD_PRODUCT_SET_LOADING";

export const state = () => ({
	content: [],
	totals: {},
	loading: {
		content: true,
		totals: true,
		addProduct: false,
		update: false,
	},
});

export const mutations = {
	[GET_CONTENT](state, content) {
		state.content = content;
	},

	[GET_CONTENT_SET_LOADING](state, loading) {
		state.loading.content = loading;
	},

	[GET_TOTALS](state, totals) {
		state.totals = totals;
	},

	[GET_TOTALS_SET_LOADING](state, loading) {
		state.loading.totals = loading;
	},

	[ADD_PRODUCT](state, content) {
		state.content = { ...state.content, ...{ [content.key]: content } };
	},

	[ADD_PRODUCT_SET_LOADING](state, { loading }) {
		state.loading.addProduct = loading;
	},

	// [UPDATE_PRODUCT](state, { key, quantity }) {
	// 	const product = state.content[key];
	// 	product.quantity = quantity;
	// },

	// [REMOVE_PRODUCT](state, { key }) {
	// 	Vue.delete(state.content, key);
	// },

	// [CRUD_PRODUCT_SET_LOADING](state, { key, loading }) {
	// 	Vue.set(state.content[key], "loading", loading);
	// },


};

export const actions = {
	async getContent({ commit }, { $axios = this.$axios, error = console.error } = {}) {
		try {
			commit(GET_CONTENT_SET_LOADING, true);
			const content = await $axios.$get("/wc-legacy/cart");
			if (typeof content === "object") commit(GET_CONTENT, content);
			commit(GET_CONTENT_SET_LOADING, false);
		} catch (e) {
			error(e);
		}
	},

	async getTotals({ commit }, { $axios = this.$axios, error = console.error } = {}) {
		try {
			commit(GET_TOTALS_SET_LOADING, true);
			const totals = await $axios.$get("/wc-legacy/cart/totals");
			commit(GET_TOTALS, totals);
			commit(GET_TOTALS_SET_LOADING, false);
		} catch (e) {
			error(e);
		}
	},

	async crudProduct({ commit, dispatch }, {
		callback,
		error = console.error,
	}) {
		commit(GET_CONTENT_SET_LOADING, true);

		try {
			await callback;
		} catch (e) {
			error(e);
		}

		await Promise.all([dispatch("getTotals"), dispatch("getContent")]);
		commit(GET_CONTENT_SET_LOADING, false);
	},

	async addProduct({ commit, dispatch }, {
		product,
		$axios = this.$axios,
		error = console.error,
	} = {}) {
		commit(ADD_PRODUCT_SET_LOADING, { loading: true });
		await dispatch("crudProduct", {
			callback: $axios.$post("/wc-legacy/cart/add", product),
			error,
		});
		commit(ADD_PRODUCT_SET_LOADING, { loading: false });
	},

	async updateProduct({ commit, dispatch }, {
		product: { key },
		quantity = 1,
		$axios = this.$axios,
		error = console.error,
	} = {}) {
		commit(GET_CONTENT_SET_LOADING, true);

		try {
			await $axios.$post("/wc-legacy/cart/cart-item", { cart_item_key: key, quantity });
		} catch (e) {
			error(e);
		}

		await Promise.all([dispatch("getTotals"), dispatch("getContent")]);
		commit(GET_CONTENT_SET_LOADING, false);
	},

	async removeProduct({ commit, dispatch }, {
		product: { key },
		$axios = this.$axios,
		error = console.error,
	} = {}) {
		commit(GET_CONTENT_SET_LOADING, true);

		try {
			await $axios.$delete("/wc-legacy/cart/cart-item", { params: { cart_item_key: key } });
		} catch (e) {
			error(e);
		}

		await Promise.all([dispatch("getTotals"), dispatch("getContent")]);
		commit(GET_CONTENT_SET_LOADING, false);
	},
};

export const getters = {
	content: ({ content }, getters, rootState, rootGetters) => Object.keys(content).map(objectKey => ({
		...content[objectKey],
		productData: rootGetters["product/byId"](content[objectKey].productId),
	})),
	loading: state => state.loading,
	totals: state => state.totals,
	count: state => Object.keys(state.content).length,
};
