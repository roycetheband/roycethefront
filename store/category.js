/* eslint-disable no-shadow, no-param-reassign, no-console */
export const SET_CATEGORIES = "SET_CATEGORIES";
export const TOGGLE_CATEGORY_MENU_VISIBILITY = "TOGGLE_CATEGORY_MENU_VISIBILITY";

export const state = () => ({
	list: [],
	menu: {
		visible: false,
		allowedRoutes: [
			"shop",
			"shop-product-slug",
			"shop-category-slug",
		],
	},
});

export const mutations = {
	[SET_CATEGORIES](state, categories) {
		state.list = categories;
	},

	[TOGGLE_CATEGORY_MENU_VISIBILITY](state, currentRouteName) {
		state.menu.visible = state.menu.allowedRoutes.includes(currentRouteName);
	},
};

export const actions = {

	async getList({ commit }, { $axios = this.$axios, error = console.error } = {}) {
		try {
			const categories = await $axios.$get("/wc/products/categories");
			commit(SET_CATEGORIES, categories);
		} catch (e) {
			error(e);
		}
	},

	async toggleCategoryMenuVisibility({ commit }, currentRouteName) {
		commit(TOGGLE_CATEGORY_MENU_VISIBILITY, currentRouteName);
	},
};

export const getters = {
	list: state => state.list,
	byId: state => id => state.list.find(c => c.id === id),
	bySlug: state => slug => state.list.find(c => c.slug === slug),
};
