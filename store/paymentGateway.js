/* eslint-disable no-shadow, no-param-reassign, no-unused-vars, no-console */
export const GET_PAYMENT_GATEWAY = "GET_PAYMENT_GATEWAY";

export const state = () => ({
	list: [],
});

export const mutations = {
	[GET_PAYMENT_GATEWAY](state, list) {
		state.list = list;
	},
};

export const actions = {
	async getList({ commit }, { $axios = this.$axios, error = console.error } = {}) {
		try {
			const list = await $axios.$get("/wc/payment_gateways");
			commit(GET_PAYMENT_GATEWAY, list);
		} catch (e) {
			error(e);
		}
	},
};

export const getters = {
	list: state => state.list,
	listEnabled: state => state.list.filter(({ enabled }) => enabled),
	byId: state => id => state.list.find(c => c.id === id),
	bySlug: state => slug => state.list.find(c => c.slug === slug),
};
