/* eslint-disable import/prefer-default-export */

export const state = () => ({
	primary: [
		{
			routeName: "shop",
			title: "Shop",
			icon: "shirt",
		}, {
			routeName: "shows",
			title: "Shows",
			icon: "beer",
		}, {
			routeName: "music",
			title: "Music",
			icon: "headphones",
		},
	],
	legal: [
		{
			routeName: "faq",
			title: "Shipping & Returns",
		}, {
			routeName: "privacy",
			title: "Privacy Policy",
		}, {
			routeName: "terms-and-conditions",
			title: "Terms & Conditions",
		}, {
			routeName: "disclaimer",
			title: "Disclaimer",
		},
	],
});
