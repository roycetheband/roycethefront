/* eslint-disable no-shadow, no-param-reassign, no-console */
export const SET_CATEGORIES = "SET_CATEGORIES";
export const TOGGLE_CATEGORY_MENU_VISIBILITY = "TOGGLE_CATEGORY_MENU_VISIBILITY";

export const state = () => ({
	general: {
		language: "nl",
		currency: "EUR",
	},
});
