export const state = () => {};

export const actions = {
	async nuxtServerInit({ dispatch }, { store, app: { $axios }, error }) {
		/* Categories */
		await dispatch("category/toggleCategoryMenuVisibility", store.$router.currentRoute.name);

		if (store.state.category.menu.visible) {
			await dispatch("category/getList", { $axios, error });
		}

		/* Products */
		await dispatch("product/getList");
	},
};
