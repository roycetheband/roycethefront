/* eslint-disable no-shadow, no-param-reassign, no-unused-vars, no-console */
export const GET_PRODUCTS = "GET_PRODUCTS";

export const state = () => ({
	list: [],
});

export const mutations = {
	[GET_PRODUCTS](state, products) {
		state.list = products;
	},
};

export const actions = {
	async getList({ commit }, { $axios = this.$axios, error = console.error } = {}) {
		try {
			const products = await $axios.$get("/wc/products");
			commit(GET_PRODUCTS, products);
		} catch (e) {
			error(e);
		}
	},
};

export const getters = {
	list: state => state.list,
	listByCategoryId: state => categoryId => state.list.filter(p => p.categories.some(c => c.id === categoryId)),
	byId: state => id => state.list.find(c => c.id === id),
	bySlug: state => slug => state.list.find(c => c.slug === slug),
	stockById: state => (id) => {
		const {
			inStock,
			manageStock,
			stockQuantity,
			backordersAllowed,
		} = state.list.find(c => c.id === id);

		return {
			inStock,
			manageStock,
			stockQuantity,
			backordersAllowed,
		};
	},
	orderableById: state => (id) => {
		const { stockStatus } = state.list.find(c => c.id === id);
		return stockStatus === "instock" || stockStatus === "onbackorder";
	},
};
