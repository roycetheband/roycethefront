/* eslint-disable no-shadow, no-param-reassign */
export const SET_APP_HEADER_HEIGHT = "SET_APP_HEADER_HEIGHT";

export const state = () => ({
	appHeaderHeight: 0,
});

export const mutations = {
	[SET_APP_HEADER_HEIGHT](state, height) {
		state.appHeaderHeight = height;
	},
};

export const actions = {
	async setAppHeaderHeight({ commit }, height) {
		commit(SET_APP_HEADER_HEIGHT, height);
	},
};

export const getters = {
	appHeaderHeight: state => state.appHeaderHeight,
};
