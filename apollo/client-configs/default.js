import { ApolloLink } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

export default () => {
	const httpLink = new HttpLink({ uri: process.env.GRAPHQL_URL });
	const middlewareLink = new ApolloLink((operation, forward) => forward(operation));
	const link = middlewareLink.concat(httpLink);
	return {
		link,
		cache: new InMemoryCache(),
	};
};
