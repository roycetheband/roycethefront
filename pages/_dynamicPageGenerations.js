/*

Create dynamic routes

const params = {
	first: "shop",
	second: "checkout",
	third: "overview",
}

"/:first/:second/:third/:fourth/:fifth"

NOPE -> Count params, loop to check if last has all parents and if last one has no parent (if it does it means that it is another nested page)
YES -> Can search page on uri

OK -> okido, get the content
nOK -> 404

*/
