module.exports = {
	root: true,
	env: {
		browser: true,
		node: true,
	},
	parserOptions: {
		parser: "babel-eslint",
	},
	extends: ["airbnb-base", "plugin:vue/strongly-recommended"],
	plugins: ["vue", "graphql"],
	rules: {
		"max-len": 0,
		"no-tabs": 0,
		indent: ["error", "tab"],
		quotes: [2, "double"],
		"vue/html-indent": [
			"error",
			"tab",
			{
				attribute: 1,
				closeBracket: 0,
				alignAttributesVertically: true,
				ignores: [],
			},
		],
		"linebreak-style": 0,
		"no-underscore-dangle": ["error", { allow: ["__NUXT__"] }],
		"graphql/template-strings": [
			"error",
			{
				env: "literal",
			},
		],
	},
	globals: {},
	settings: {
		"import/resolver": {
			webpack: "webpack.config.js",
		},
		"import/core-modules": [
			"apollo-link",
			"apollo-link-http",
			"apollo-cache-inmemory",
			"vuex",
			"vue"
		],
	},
};
