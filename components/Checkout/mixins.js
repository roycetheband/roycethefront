import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

const Btn = () => import("~/components/Btn.vue");
const Country = () => import("~/components/Country/Name.vue");
const CountrySelect = () => import("~/components/Country/Select.vue");

export default {
	components: {
		FontAwesomeIcon,
		Btn,
		Country,
		CountrySelect,
	},

	data: () => ({
		faChevronRight,
	}),

	props: {
		editable: {
			type: Boolean,
			default: false,
		},
	},

	methods: {
		submit() {
			this.$emit("submit");
		},

		edit() {
			this.$emit("edit");
		},
	},
};
