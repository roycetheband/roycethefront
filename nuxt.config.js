import postcssPresetEnv from "postcss-preset-env"; // eslint-disable-line import/no-extraneous-dependencies
import StylelintPlugin from "stylelint-webpack-plugin"; // eslint-disable-line import/no-extraneous-dependencies
import breakpoints, { customMediaQueriesBreakpoints } from "./breakpoints";

require("dotenv").config(); // eslint-disable-line import/no-extraneous-dependencies


export default {

	head: {
		title: "roycethefront",
		meta: [
			{ charset: "utf-8" },
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1",
			},
			{
				hid: "description",
				name: "description",
				content: "Nuxt front for a Headless Wordpress Woocommerce",
			}, {
				"http-equiv": "Accept-CH",
				content: "DPR, Viewport-Width, Width",
			},

		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
	},

	loading: { color: "rgba(255, 107, 107, 1)" },

	css: [
		"node_modules/modern-normalize/modern-normalize.css",
		"node_modules/css-modularscale/index.css",
		"~/assets/styles/global.scss",
	],

	modules: [
		"@nuxtjs/dotenv",
		"@nuxtjs/proxy",
		"@nuxtjs/axios",
		"nuxt-mq",
		["bootstrap-vue/nuxt", { css: false }],
	],

	axios: {
		proxy: true,
	},

	proxy: {
		"/wc": {
			target: process.env.WP_URL,
			auth: `${process.env.WC_CONSUMER_KEY}:${
				process.env.WC_CONSUMER_SECRET
			}`,
			pathRewrite: {
				"^/wc-legacy": "/wp-json/wc/v2",
				"^/wc": "/wp-json/wc/v3",
			},
			secure: process.env.WC_PROXY_SECURE === "true",
		},
	},

	mq: {
		breakpoints,
	},

	plugins: [
		"~/plugins/async-computed.js",
		"~/plugins/vue-cloudindary.js",
		"~/plugins/vue-observe-visibility.js",
		"~/plugins/vue-font-awesome.js",
		"~/plugins/axios.js",
		"~/plugins/vee-validate.js",
		{ src: "~/plugins/vue-select.js", ssr: false },
	],

	/*
	** Build configuration
	*/
	build: {

		postcss: [
			postcssPresetEnv({
				stage: 0,
				importFrom: [{
					customMedia: customMediaQueriesBreakpoints,
				}],
			}),
		],

		extend(config, { isDev }) {
			/* CSS modules */
			if (isDev) {
				const vueLoader = config.module.rules.find(rule => rule.loader === "vue-loader");
				vueLoader.options.cssModules = {
					...vueLoader.options.cssModules,
					localIdentName: "[local]",
				};
			}

			/* vue-svg-loader */
			const imageLoaderRule = config.module.rules.find(rule => rule.test && /svg/.test(rule.test.toString()));
			imageLoaderRule.test = /\.(png|jpe?g|gif|webp)$/;
			config.module.rules.push({
				test: /\.svg$/,
				loader: "vue-svg-loader",
				options: {
					svgo: {
						plugins: [
							{ removeDoctype: true },
							{ removeComments: true },
							{ removeViewBox: false },
							{ removeDimensions: true },
						],
					},
				},
			});

			/* Stylelint */
			config.plugins.push(new StylelintPlugin({
				files: ["**/*.vue", "**/*.s?(a|c)ss"],
				fix: true,
			}));

			/* eslint-loader */
			if (isDev && process.client) {
				config.module.rules.push({
					enforce: "pre",
					test: /\.(js|vue)$/,
					loader: "eslint-loader",
					exclude: /(node_modules)/,
				});
			}
		},
	},
};
