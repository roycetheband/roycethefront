import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faBandcamp,
	faInstagram,
	faFacebookSquare,
	faTwitter,
	faYoutube,
	faSpotify,
	faItunesNote,
} from "@fortawesome/free-brands-svg-icons";

library.add(
	faBandcamp,
	faInstagram,
	faFacebookSquare,
	faTwitter,
	faYoutube,
	faSpotify,
	faItunesNote,
);
