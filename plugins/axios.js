import applyConverters from "axios-case-converter";

export default ({ $axios }) => {
	applyConverters($axios);
};
