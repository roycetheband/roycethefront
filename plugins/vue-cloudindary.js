/* global MozNamedAttrMap */
/* eslint-disable prefer-destructuring, no-param-reassign */

import Vue from "vue";
import cloudinary from "cloudinary-core";

const version = "0.1.0";
let cloudinaryInstance;

const compatible = (/^2\./).test(Vue.version);
if (!compatible) {
	Vue.util.warn(`CloudinaryImage ${version} only supports Vue 2.x, and does not support Vue ${Vue.version}`);
}

const cloudinaryAttr = (attr) => {
	let attribute = attr;
	if (attr.match(/cl[A-Z]/)) attribute = attr.substring(2);
	return attribute.replace(/([a-z])([A-Z])/g, "$1_$2").toLowerCase();
};

/**
 * Returns an array of attributes for cloudinary.
 * @function toCloudinaryAttributes
 * @param {Object} source - an object containing attributes
 * @param {(RegExp|string)} [filter] - copy only attributes whose name matches the filter
 * @return {Object} attributes for cloudinary functions
 */
const toCloudinaryAttributes = (source, filter) => {
	const attributes = {};
	let isNamedNodeMap;
	if (window.NamedNodeMap) {
		isNamedNodeMap = source && (source.constructor.name === "NamedNodeMap" || source instanceof NamedNodeMap);
	} else if (window.MozNamedAttrMap) {
		// https://developer.mozilla.org/en-US/docs/Web/API/NamedNodeMap
		// https://www.fxsitecompat.com/en-CA/docs/2013/namednodemap-has-been-renamed-to-moznamedattrmap/
		// In Firefox versions 22 - 33 the interface "NamedNodeMap" was called "MozNamedAttrMap"
		isNamedNodeMap = source && (source.constructor.name === "MozNamedAttrMap" || source instanceof MozNamedAttrMap);
	}

	Array.prototype.forEach.call(source, (value, name) => {
		if (isNamedNodeMap) {
			name = value.name;
			value = value.value;
		}
		if (!filter || filter.exec(name)) {
			attributes[cloudinaryAttr(name)] = value;
		}
	});

	return attributes;
};

const getPublicId = (value) => {
	if (value.includes("cloudinary.com")) {
		const regex = /^.+\.cloudinary\.com\/(?:[^/]+\/)(?:(image|video)\/)?(?:(upload|fetch)\/)?(?:(?:[^_/]+_[^,/]+,?)*\/)?(?:s--(.*)--\/)?(?:v(\d+|\w{1,2})\/)?([^.^\s]+)(?:\.(.+))?$/;
		const result = regex.exec(value);
		return result ? `${result[5]}.${result[6]}` : value;
	}

	return value;
};

const loadImage = (el, value, options) => {
	if (options.responsive === "" || options.responsive === "true" || options.responsive === true) {
		options.responsive = true;
	}

	const publicId = getPublicId(value);
	let url = cloudinaryInstance.url(publicId, options);
	url = url.replace(/^http:\/\//i, "https://");

	if (options.responsive) {
		cloudinaryInstance.Util.setData(el, "src", url);
		cloudinaryInstance.cloudinary_update(el, options);
		cloudinaryInstance.responsive(options, false);
	} else {
		el.setAttribute("src", url);
	}
};

const clImage = {
	inserted(el, binding) {
		const options = toCloudinaryAttributes(el.attributes);

		if (el.attributes.htmlWidth) {
			el.setAttribute("width", el.attributes.htmlWidth);
		} else {
			el.removeAttribute("width");
		}
		if (el.attributes.htmlHeight) {
			el.setAttribute("height", el.attributes.htmlHeight);
		} else {
			el.removeAttribute("height");
		}
		loadImage(el, binding.value, options);
	},
};

const configuration = new cloudinary.Configuration({
	cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
	api_key: process.env.CLOUDINARY_API_KEY,
});

cloudinaryInstance = new cloudinary.Cloudinary(configuration.config());
cloudinary.Util.assign(cloudinaryInstance, cloudinary); // copy namespace to the service instance

Vue.directive("cl-image", clImage);
